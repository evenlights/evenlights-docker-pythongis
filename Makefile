.PHONY: clean clean-docker build tag push

HUB_USER := evenlights
REPO_NAME := pythongis
VERSION := 1.1.1
IMAGE_LATEST := $(HUB_USER)/$(REPO_NAME):latest
IMAGE_VERSION := $(HUB_USER)/$(REPO_NAME):$(VERSION)


clean: clean-docker

clean-docker: ##
	docker rmi $(IMAGE_LATEST)

build:
	docker build -t $(IMAGE_LATEST) .

tag:
	docker tag $(IMAGE_LATEST) $(IMAGE_VERSION)

login:
	docker login

push:
	docker push $(IMAGE_LATEST)
	docker push $(IMAGE_VERSION)
