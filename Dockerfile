FROM evenlights/python3:latest

MAINTAINER Evenlights Studio <development@evenlights.com>

ENV CPLUS_INCLUDE_PATH /usr/include/gdal
ENV C_INCLUDE_PATH /usr/include/gdal
ENV IMAGE_VERSION 1.1.1

RUN set -ex \
    && gisDeps=" \
        libproj-dev \
        gdal-bin \
        libgdal-dev \
        python-gdal \
        python3-gdal \
        libgeoip1 \
        libgeos-dev \
        libgeos-c1v5 \
    " \
    && apt-get update \
    && apt-get install -y --no-install-recommends $gisDeps \
    \
    && rm -rf /var/lib/apt/lists/*
