=======
History
=======


1.1.1 (2018-06-17)
------------------

Rebuilt against `python3:1.1.1`. 


1.1.0 (2018-06-17)
------------------

Rebuilt against `python3:1.1.0`. 


1.0.0 (2018-06-09)
------------------

Initial release. Support for uwsgi and nginx servers. 
