Python 3 GIS Base Image
=======================
* Source: [Butbucket](https://bitbucket.org/evenlights/evenlights-docker-pythongis/)
* Size: 765MB
* System: Ubuntu 18.04


Packages
--------
* GDAL 2.2.3+dfsg-2
* GEOS 3.6.2-1build2
* PROJ.4 4.9.3-2
* GeoIP 1.6.12-1

The image is configured for the UTC timezone. 


Deployment Structure 
--------------------
* application code is found in `/app`
* static is served from `/app/static`, media from `/app/media` respectively

Build your target image
-----------------------
Most likely you'll want to do something like that to build your image:

```dockerfile
FROM evenlights/pythongis:latest

WORKDIR /app
    
ADD /etc/pip/requirements.txt /app/
    
RUN pip install -r requirements.txt

RUN set -ex \
    && buildDeps=" \
        binutils \
        build-essential \
    " \
    && apt-get update \
    && apt-get install -y $buildDeps --no-install-recommends \
    \
    && pip3 install -r requirements.txt \
    \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge -y --auto-remove $buildDeps
```

And then use a snippet like that to run your app:

```docker
  app:
    image: your/image
    command: gunicorn main_api.wsgi:application -w 3 -b 0.0.0.0:8000 --max-requests 1000
    working_dir: /app/src
    env_file:
      - ./env/app.env
      - ./env/db.env
    volumes:
      - ./../../data/static:/static
      - ./../../data/media:/media
    expose:
      - 8000
    depends_on:
      - postgres
      - redis
```


Release flow
------------

create a release branch, desirably using a *flow plugin

build the image:

    $ make build

bump the version:

    $ bumpversion {major|minor|patch}

tag the image:

    $ make tag

login tp your repo, note that this will create file under 
`$HOME/.docker/config.json`:

    $ make login

push the image:

    $ make push

update ``HISTORY.md`` describing changes that this release introduces

publish the branch

create a pull request into `default`

finish the release using *flow
